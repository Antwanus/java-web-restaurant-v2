package com.virtualpairprogrammers.web.servlets;

import com.virtualpairprogrammers.data.MenuDao;
import com.virtualpairprogrammers.data.MenuDaoFactory;
import org.json.JSONObject;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/updatedStatus")
public class OrderStatusAjaxServlet extends HttpServlet {

   @Override
   protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
      Long orderId = Long.valueOf(req.getParameter("orderId"));
      MenuDao menuDao = MenuDaoFactory.getMenuDao();
      String orderStatus = menuDao.getOrderById(orderId).getStatus();

      PrintWriter out = resp.getWriter();
      resp.setContentType("text/html");

      JSONObject json = new JSONObject();
      json.put("orderStatus", orderStatus);
      SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
      json.put("time", sdf.format(new Date()));

      out.write(json.toString());

      out.close();
   }
}