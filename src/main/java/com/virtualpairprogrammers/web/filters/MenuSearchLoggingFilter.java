package com.virtualpairprogrammers.web.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebFilter(value = {"/searchResults.html", "/anotherUrl.html"})
public class MenuSearchLoggingFilter implements Filter {
   static Logger logger = Logger.getLogger("com.virtualpairprogrammers");
   @Override
   public void init(FilterConfig filterConfig) throws ServletException {

   }

   @Override
   public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
      String searchTerm = request.getParameter("searchTerm");
      logger.log( Level.INFO, "searchTerm: {0}", searchTerm);
      chain.doFilter(request, response);
   }

   @Override
   public void destroy() {

   }
}
