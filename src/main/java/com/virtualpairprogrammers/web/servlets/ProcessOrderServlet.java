package com.virtualpairprogrammers.web.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.virtualpairprogrammers.data.MenuDao;
import com.virtualpairprogrammers.data.MenuDaoFactory;
import com.virtualpairprogrammers.domain.Order;
import com.virtualpairprogrammers.web.websockets.KitchenDisplaySessionHandler;
import com.virtualpairprogrammers.web.websockets.KitchenDisplaySessionHandlerFactory;

@WebServlet("/processorder.html")
public class ProcessOrderServlet extends HttpServlet{
	static final Logger logger = Logger.getLogger("com.virtualpairprogrammers.web");

	@Override
	public void doGet (HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		MenuDao menuDao = MenuDaoFactory.getMenuDao();
		List<Order> orders;
		orders = menuDao.getAllOrders();
		request.setAttribute("orders", orders);

		List<String> statuses = new ArrayList<>();
		statuses.add("order accepted");
		statuses.add("payment received");
		statuses.add("being prepared");
		statuses.add("ready for collection");

		request.setAttribute("statuses", statuses);

		ServletContext context = getServletContext();
		RequestDispatcher dispatch = context.getRequestDispatcher("/processorder.jsp");
		dispatch.forward(request,response);
	}

	@Override
	public void doPost (HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		MenuDao menuDao = MenuDaoFactory.getMenuDao();
		Long id = Long.valueOf(request.getParameter("id")); //NumberFormatException
		String status =  request.getParameter("status");
		logger.log(Level.INFO, "{0} : {1} ", new Object[]{id, status});
		menuDao.updateOrderStatus(id,status);

		Order order = menuDao.getOrderById(id);
		KitchenDisplaySessionHandler kDSHandler = KitchenDisplaySessionHandlerFactory.getHandler();
		kDSHandler.amendOrder(order);

		doGet(request,response);
	}
}
