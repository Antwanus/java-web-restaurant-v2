package com.virtualpairprogrammers.web.servlets;

import com.virtualpairprogrammers.data.MenuDao;
import com.virtualpairprogrammers.data.MenuDaoFactory;
import com.virtualpairprogrammers.domain.Order;

import javax.servlet.AsyncContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class KitchenAsyncRunnableTask implements Runnable{

   private AsyncContext asyncContext;

   public void setAsyncContext(AsyncContext asyncContext) { this.asyncContext = asyncContext; }

   @Override
   public void run() {
      HttpServletRequest req = (HttpServletRequest) asyncContext.getRequest();
      HttpServletResponse resp = (HttpServletResponse) asyncContext.getResponse();

      PrintWriter out = null;
      try {
         out = resp.getWriter();
      } catch (IOException e) {
         e.printStackTrace();
         asyncContext.complete();
         throw new RuntimeException(e);
      }
      resp.setContentType("text/html");
      long reqParamSize = Long.parseLong(req.getParameter("size"));
      MenuDao menuDao = MenuDaoFactory.getMenuDao();

      while (menuDao.getAllOrders().size() < reqParamSize) {
         try {
            Thread.sleep(500);
         } catch (InterruptedException e) {
            e.printStackTrace();
            asyncContext.complete();
            throw new  RuntimeException(e);
         }
      }

      Order order = menuDao.getOrderById(reqParamSize);

      out.println("<p><strong>NEXT ORDER ("+order.getId()+")</strong><br/>"+ order.toString() +"</p>");
      out.close();

      asyncContext.complete();
   }

}
