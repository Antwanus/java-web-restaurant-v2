package com.virtualpairprogrammers.web.servlets;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(value = "/kitchenAsyncServlet", asyncSupported = true)
public class KitchenAsyncServlet extends HttpServlet {

   @Override
   protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
      AsyncContext asyncContext = req.startAsync(req, resp);   // Tomcat is assigning another http worker thread instead of using a different threadpool.. So actually.. this is pointless
      KitchenAsyncRunnableTask task = new KitchenAsyncRunnableTask();
      task.setAsyncContext(asyncContext);

      asyncContext.start(task);

   }
}
