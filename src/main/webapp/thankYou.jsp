<%@ taglib  prefix="c"      uri="http://java.sun.com/jsp/jstl/core"     %>
<%@ taglib  prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt"      %>

<html>
<head>
    <script>
        function updateOrderStatus() {
            var request = new XMLHttpRequest();
            request.onreadystatechange = function() {
                if (this.readyState == 4) {
                    var json = JSON.parse(this.responseText);
                    document.getElementById("orderStatus").innerHTML = json.orderStatus;
                    document.getElementById("time").innerHTML = "Last updated: " + json.time;
                }
            }
            request.open("GET", "/updatedStatus?orderId=${orderId}", true);
            request.send();
        }
        
        window.setInterval(
            function() {
                updateOrderStatus();
            }, 
            2000
        );
    </script>
</head>
    <body>
        <jsp:include page="/header.jsp" />
        <h2>Thank you for your patronage</h2>

        <p>Order received, thank you! Grand total:
        <fmt:formatNumber value="${total}" type="currency" currencyCode="${currency}" /></p>

        <p>
            The current status of your order is: [ <span id="orderStatus">${orderStatus}</span> ]
            <input type="button" value="Check Status" onclick="updateOrderStatus()"/>
        </p>
        <p id="time"></p>

        <jsp:include page="/footer.jsp"/>
    </body>
</html>