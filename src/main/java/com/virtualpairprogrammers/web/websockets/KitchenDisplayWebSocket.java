package com.virtualpairprogrammers.web.websockets;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.util.logging.Level;
import java.util.logging.Logger;

@ServerEndpoint("/kitchenManagement")
public class KitchenDisplayWebSocket {

   static final Logger logger = Logger.getLogger("com.virtualpairprogrammers.web");

   @OnOpen
   public void onOpen(Session session) {
      KitchenDisplaySessionHandler sessionHandler = KitchenDisplaySessionHandlerFactory.getHandler();
      sessionHandler.addSession(session);
   }
   @OnClose
   public void onClose(Session session) {
      KitchenDisplaySessionHandler sessionHandler = KitchenDisplaySessionHandlerFactory.getHandler();
      sessionHandler.removeSession(session);
   }
   @OnError
   public void onError(Throwable throwable) {
      logger.log(Level.WARNING, "KitchenDisplayWebSocket.onError():  <AN ERROR HAS OCCURRED> ");
      throw new RuntimeException(throwable);
   }

}
