package com.virtualpairprogrammers.web.servlets;

import com.virtualpairprogrammers.data.MenuDao;
import com.virtualpairprogrammers.data.MenuDaoFactory;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/thankYou.html")
@ServletSecurity(
	@HttpConstraint(rolesAllowed = {"user"})
)
public class ThankYouServlet extends HttpServlet {
	@Override
	public void doGet (HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		Long orderId = (Long) session.getAttribute("orderId");

		MenuDao menuDao = MenuDaoFactory.getMenuDao();
		Double total = menuDao.getOrderTotal(orderId);
		String orderStatus = menuDao.getOrderById(orderId).getStatus();

		if (total == null) {
			response.sendRedirect("/order.html");
			return;
		}

		request.setAttribute("orderId", orderId);
		request.setAttribute("total", total);
		request.setAttribute("currency", "USD");
		request.setAttribute("orderStatus", orderStatus);

		ServletContext servletContext = getServletContext();
		RequestDispatcher requestDispatcher = servletContext.getRequestDispatcher("/thankYou.jsp");
		requestDispatcher.forward(request, response);
	}
}
