package com.virtualpairprogrammers.web.filters;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class MenuSearchCorrectionRequestWrapper extends HttpServletRequestWrapper {
   private String newSearchTerm;

   /**
    * Constructs a request object wrapping the given request.
    *
    * @param request The request to wrap
    * @throws IllegalArgumentException if the request is null
    */
   public MenuSearchCorrectionRequestWrapper(HttpServletRequest request) {
      super(request);
   }

   public void setNewSearchTerm(String newSearchTerm) {
      this.newSearchTerm = newSearchTerm;
   }
   @Override
   public String getParameter(String name) {
      if (name.equals("searchTerm")) {
         return newSearchTerm;
      }
      else return super.getParameter(name);
   }

}
