package com.virtualpairprogrammers.web.websockets;

import com.virtualpairprogrammers.domain.Order;
import org.json.JSONObject;

import javax.websocket.EncodeException;
import javax.websocket.Session;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class KitchenDisplaySessionHandler {

   private List<Session> sessions = new ArrayList<>();

   public void addSession(Session session) {
      sessions.add(session);
   }
   public void removeSession(Session session) {
      sessions.remove(session);
   }

   private void sendMessage(JSONObject msg) {
      for (Session s : sessions) {
         try {
            s.getBasicRemote().sendText(msg.toString());
         } catch (IOException e) {
            e.printStackTrace();
            removeSession(s);
         }
   }}

   public void newOrder(Order order) {
      JSONObject json = new JSONObject();
      json.append("id", order.getId().toString() );
      json.append("status", order.getStatus() );
      json.append("content", order.toString() );
      json.append("action", "add");
      json.append("update", new Date().toString() );
      sendMessage(json);
   }

   public void amendOrder(Order order) {
      JSONObject json = new JSONObject();
      json.append("id", order.getId().toString());
      json.append("action", "remove");
      sendMessage(json);
      newOrder(order);
   }
}
